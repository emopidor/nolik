function bind_nolik(nolik_element, audio_element) {
    var audio_ctx = new (window.AudioContext || window.webkitAudioContext)();
    var analyser = audio_ctx.createAnalyser();
    var audio_source = audio_ctx.createMediaElementSource(audio_element);
    audio_source.connect(analyser);
    audio_source.connect(audio_ctx.destination);

    var buffer_length = analyser.frequencyBinCount;
    var frequencies = new Uint8Array(buffer_length);

    var pixels = nolik_element.querySelectorAll(".pixel");

    var mult = 197;

    function draw(event) {
        analyser.getByteTimeDomainData(frequencies);
        var to_activate = new Set();
        var step = Math.max(1, Math.floor(frequencies.length / Math.max(1, pixels.length)));

        for (var i = 0; i < frequencies.length; i += step) {
            if (frequencies[i] > 190) {
                to_activate.add((i * mult / step) % pixels.length);
            }
        }

        for (var i = 0; i < pixels.length; i++) {
            if (to_activate.has(i)) {
                pixels[i].classList.add("activated");
            } else {
                pixels[i].classList.remove("activated");
            }
        }

        window.setTimeout(draw, 100);
    }

    draw(null);
}
